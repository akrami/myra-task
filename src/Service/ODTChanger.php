<?php


namespace App\Service;

class ODTChanger
{
    private $file;

    /**
     * ODTChanger constructor.
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * Generate an ODT file from template
     * @param $header
     * @param $senderAddress
     * @param $address1
     * @param $address2
     * @param $address3
     * @param $address4
     * @param $senderPhone
     * @param $senderFax
     * @param $senderEmail
     * @param $dueDate
     * @param $signature
     * @param $domain
     * @param $month
     * @param $count
     * @param $fee
     * @param $mwst
     * @param $renr
     * @param $redate
     * @param $total
     * @return bool
     */
    public function generate(
        $header,
        $senderAddress,
        $address1,
        $address2,
        $address3,
        $address4,
        $senderPhone,
        $senderFax,
        $senderEmail,
        $dueDate,
        $signature,
        $domain,
        $month,
        $count,
        $fee,
        $mwst,
        $renr,
        $redate,
        $total
    )
    {
        // Checking for validity
        if (file_exists($this->file) && is_readable($this->file)){
            // copy from template
            $newFile = preg_replace('/\/[\w\_]+\.odt/', '/'.$domain .'.odt', $this->file);
            copy($this->file, $newFile);
            // odt is exactly zip
            $zip = new \ZipArchive();
            if ($zip->open($newFile)===true){
                $contents = $zip->getFromName('content.xml');
                // start replacing text
                $contents = str_replace('%HEADER%', $header, $contents);
                $contents = str_replace('%ADDRESS%', $senderAddress, $contents);
                $contents = str_replace('%ANSCHRIFT-ZEILE1%', $address1, $contents);
                $contents = str_replace('%ANSCHRIFT-ZEILE2%', $address2, $contents);
                $contents = str_replace('%ANSCHRIFT-ZEILE3%', $address3, $contents);
                $contents = str_replace('%ANSCHRIFT-ZEILE4%', $address4, $contents);
                $contents = str_replace('%PHONE%', $senderPhone, $contents);
                $contents = str_replace('%FAX%', $senderFax, $contents);
                $contents = str_replace('%EMAIL%', $senderEmail, $contents);
                $contents = str_replace('%REDATUM%', $redate, $contents);
                $contents = str_replace('%RENR%', $renr, $contents);
                $contents = str_replace('%DOMAIN%', $domain, $contents);
                $contents = str_replace('%MONAT%', $month, $contents);
                $contents = str_replace('%EINHEITSPREIS%', number_format($fee, 2)." EUR", $contents);
                $contents = str_replace('%ANZAHL%', $count, $contents);
                $contents = str_replace('%NETTOMYRA%', number_format($total,2)." EUR", $contents);
                $mwst = intval($mwst)*intval($total)/100;
                $contents = str_replace('%MWST%', number_format($mwst,2)." EUR", $contents);
                $endTotal = $mwst + $total;
                $contents = str_replace('%GESAMTSUMME%', number_format($endTotal, 2)." EUR", $contents);
                $contents = str_replace('%DUEDATE%', $dueDate, $contents);
                $contents = str_replace('%SIGNATURE%', $signature, $contents);

                // replacing new file in odt
                $zip->deleteName('content.xml');
                $zip->addFromString('content.xml', $contents);
                $zip->close();

                return true;
            }
        }

        return false;
    }

}