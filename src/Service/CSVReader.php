<?php


namespace App\Service;


class CSVReader
{
    private $file;

    /**
     * CSVReader constructor.
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * Convert CSV file to associative array
     * @return array
     */
    public function convertCSVToArray()
    {
        $fileReader = new FileReader($this->file);
        $lines = $fileReader->readAsArray();

        // extracting the header from the first row of csv file
        $headers = explode(";", $lines[0]);
        $headers = array_map(function ($str) {
            return trim(preg_replace("/\"/", "", $str));
        }, $headers);
        unset($lines[0]);

        // getting the content
        $content = array();
        foreach ($lines as $line) {
            $fields = explode(";", $line);
            if (sizeof($fields) > 1 || (sizeof($fields) === 1 && strlen(trim($fields[0])) > 0)) {
                $temp = array();
                foreach ($headers as $num => $header) {
                    $fields[$num] = trim($fields[$num]);
                    // Checking field type
                    if (strpos($fields[$num], "\"") === 0 && strrpos($fields[$num], "\"") === strlen($fields[$num]) - 1) {
                        $temp[$header] = substr($fields[$num], 1, strlen($fields[$num]) - 2);
                    } elseif (is_numeric($fields[$num])) {
                        $temp[$header] = (int)$fields[$num];
                    } else {
                        $temp[$header] = $fields[$num];
                    }
                }
                array_push($content, $temp);
            }
        }

        return $content;
    }
}