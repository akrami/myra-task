<?php


namespace App\Service;


class PDFGenerator
{
    private $file;

    /**
     * PDFGenerator constructor.
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * Generate a pdf file
     * @param $header
     * @param $senderAddress
     * @param $address1
     * @param $address2
     * @param $address3
     * @param $address4
     * @param $senderPhone
     * @param $senderFax
     * @param $senderEmail
     * @param $dueDate
     * @param $signature
     * @param $domain
     * @param $month
     * @param $count
     * @param $fee
     * @param $mwst
     * @param $renr
     * @param $redate
     * @param $total
     * @return bool
     */
    public function generate(
        $header,
        $senderAddress,
        $address1,
        $address2,
        $address3,
        $address4,
        $senderPhone,
        $senderFax,
        $senderEmail,
        $dueDate,
        $signature,
        $domain,
        $month,
        $count,
        $fee,
        $mwst,
        $renr,
        $redate,
        $total
    )
    {
        $pdf = new \tFPDF();
        $pdf->AddPage();
        $pdf->SetFont('Times', '', 20);
        $pdf->SetXY(20, 28);
        $pdf->Cell(0, 0, utf8_decode($header));
        $pdf->SetFont('Arial', '', 8);
        $pdf->SetXY(20, 56);
        $pdf->Cell(0, 0, utf8_decode($senderAddress));
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(20, 65);
        $pdf->Cell(0, 0, utf8_decode($address1));
        $pdf->SetXY(20, 70);
        $pdf->Cell(0, 0, utf8_decode($address2));
        $pdf->SetXY(20, 75);
        $pdf->Cell(0, 0, utf8_decode($address3));
        $pdf->SetXY(20, 80);
        $pdf->Cell(0, 0, utf8_decode($address4));
        $pdf->SetXY(20, 90);
        $pdf->Cell(0, 0, 'Tel ' . $senderPhone, 0, 0, 'R');
        $pdf->SetXY(20, 95);
        $pdf->Cell(0, 0, 'Fax ' . $senderFax, 0, 0, 'R');
        $pdf->SetXY(20, 100);
        $pdf->Cell(0, 0, $senderEmail, 0, 0, 'R');
        $pdf->SetXY(20, 110);
        $pdf->Cell(0, 0, $redate, 0, 0, 'R');
        $pdf->SetXY(20, 120);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 0, 'Rechnung '.$renr);
        $pdf->SetXY(20, 130);
        $pdf->SetFont('Arial', 'IB', 12);
        $pdf->Cell(45, 10, 'Leistung', 1, 0, 'C');
        $pdf->Cell(45, 10, 'Einzelpreis', 1, 0, 'C');
        $pdf->Cell(45, 10, 'Anzahl', 1, 0, 'C');
        $pdf->Cell(45, 10, 'Gesamt', 1, 0, 'C');
        $pdf->SetXY(20,140);
        $pdf->SetFont('Arial', '', 12);
        $pdf->MultiCell(45,5,utf8_decode("MyraCache\nBereichung\n".$domain."\n".$month." je\n100.000 PI"), 1);
        $pdf->SetXY(65,140);
        $pdf->Cell(45,25,number_format($fee, 2)." EUR", 1, 0, 'R');
        $pdf->SetXY(110,140);
        $pdf->Cell(45,25,$count, 1, 0, 'R');
        $pdf->SetXY(155,140);
        $pdf->Cell(45,25,number_format($total, 2)." EUR", 1, 0, 'R');
        $pdf->SetXY(20,165);
        $pdf->Cell(45, 10, $mwst."% MwSt.", 1);
        $pdf->Cell(45,10, "", 1);
        $pdf->Cell(45,10, "", 1);
        $pdf->Cell(45,10, number_format($mwst*$total/100, 2)." EUR", 1, 0, 'R');
        $pdf->SetXY(20,175);
        $pdf->Cell(45, 10, "Gesamt", 1);
        $pdf->Cell(45,10, "", 1);
        $pdf->Cell(45,10, "", 1);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(45,10, number_format(($mwst*$total/100)+$total, 2)." EUR", 1, 0, 'R');
        $pdf->SetXY(20,195);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,0, utf8_decode("Zahlungsziel: ".$dueDate));
        $pdf->SetXY(20,205);
        $pdf->Cell(0,0, utf8_decode("Mit freundlichen Grüßen"));
        $pdf->SetXY(20,210);
        $pdf->Cell(0,0, utf8_decode($signature));
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetXY(20, 255);
        $pdf->MultiCell(60, 5, utf8_decode("Soprado GmbH\nBavariafilmplatz 7\n82031 Grünwald\nGeschäftsführer: Sascha Schumann"));
        $pdf->SetXY(90, 255);
        $pdf->MultiCell(60, 5, utf8_decode("Steuernummer\n143/824/16206\nAmtsgericht München,\nHRB 161675"));
        $pdf->SetXY(160, 255);
        $pdf->MultiCell(60, 5, utf8_decode("Kontoverbindung:\nMärkische Bank eG\nKonto 283 277 500\nBLZ 450 600 09"));

        $pdf->Output('F', $this->file, true);
        return true;
    }
}