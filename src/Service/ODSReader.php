<?php


namespace App\Service;


class ODSReader
{
    private $file;

    /**
     * ODSReader constructor.
     * @param string $file
     */
    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * Read an ODS file
     * @return array|null
     */
    public function read() {
        // checking validity
        if (file_exists($this->file) && is_readable($this->file)){
            // ods is exactly a zip file
            $zip = zip_open($this->file);
            if ($zip){
                while($zip_entry = zip_read($zip)) {
                    // content.xml contains the main data
                    if (zip_entry_name($zip_entry) === 'content.xml'){
                        $content = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        // loading content.xml to a XMLReader object
                        $xml = new \XMLReader();
                        $xml->XML($content);
                        $content = array();
                        $index = 0;
                        while ($xml->read()){
                            // loop through elements and check which one is a table row
                            if ($xml->name==="table:table-row" && $xml->nodeType===\XMLReader::ELEMENT){
                                while($xml->read()){
                                    if ($xml->name==="table:table-row" && $xml->nodeType===\XMLReader::END_ELEMENT){
                                        $index++;
                                        break;
                                    }
                                    if ($xml->name==="table:table-cell" && $xml->nodeType===\XMLReader::ELEMENT){
                                        if ($xml->getAttribute("table:number-columns-repeated")){
                                            $skips = intval($xml->getAttribute("table:number-columns-repeated"));
                                            for ($i=0;$i<$skips; $i++ ){
                                                $content[$index][] = null;
                                            }
                                        }else{
                                            $content[$index][] = $xml->readString();
                                        }
                                    }
                                }
                            }
                        }
                        // separating header from contents
                        $headers = $content[0];
                        unset($content[0]);
                        $result = array();
                        foreach ($content as $index=>$row) {
                            foreach ($headers as $key=>$header){
                                if ($header){
                                    $result[$index][$header] = $row[$key];
                                }
                            }
                        }

                        return $result;
                    }
                }
                return null;
            }
        }
        return null;
    }
}