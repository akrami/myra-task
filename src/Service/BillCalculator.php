<?php


namespace App\Service;

class BillCalculator
{
    private $csvFilePath;
    private $odsFilePath;


    /**
     * BillCalculator constructor.
     * @param string $csvFilePath
     * @param string $odsFilePath
     */
    public function __construct(string $csvFilePath, string $odsFilePath)
    {
        $this->csvFilePath = $csvFilePath;
        $this->odsFilePath = $odsFilePath;
    }

    /**
     * Calculating a bill and returning its data
     * @return array
     */
    public function calculate()
    {
        $csvReader = new CSVReader($this->csvFilePath);
        $contents = $csvReader->convertCSVToArray();

        $ods = new ODSReader($this->odsFilePath);
        $customers = $ods->read();

        $url = $contents[0]['URL'];
        $date = strtotime($contents[0]['Datum']);
        $months = [
            1=>"Januar",
            2=>"Februar",
            3=>"März",
            4=>"April",
            5=>"Mai",
            6=>"Juni",
            7=>"Juli",
            8=>"August",
            9=>"September",
            10=>"Oktober",
            11=>"November",
            12=>"Dezember"
        ];

        $customerData = array_filter($customers, function ($customer) use ($url) {
            return $customer['%DOMAIN%'] === $url;
        })[1];

        $fee = intval($customerData['%EINHEITSPREIS%']);

        $sum = array_reduce($contents, function ($carry, $each) {
            $carry += $each['PI/ Tag'];
            return $carry;
        }, 0);

        $count = round($sum/100000);

        return [
            "count" => $count,
            "sum" => $count*$fee,
            "year" => date('Y', $date),
            "month" => $months[intval(date('m',$date))],
            "data" => $customerData
        ];
    }

}