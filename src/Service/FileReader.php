<?php


namespace App\Service;


class FileReader
{
    private $filePath;

    /**
     * FileReader constructor.
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Read file's content
     * @return false|string|null
     */
    public function read(){
        if (file_exists($this->filePath) && is_readable($this->filePath)){
            $file = fopen($this->filePath, 'r');
            $contents = fread($file, filesize($this->filePath));
            fclose($file);
            return $contents;
        }

        return null;
    }

    public function readAsArray(){
        if (file_exists($this->filePath) && is_readable($this->filePath)){
            return file($this->filePath);
        }
        return null;
    }
}