<?php

namespace App\Command;

use App\Service\BillCalculator;
use App\Service\ODTChanger;
use App\Service\PDFGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MyracacheBillCommand extends Command
{
    protected static $defaultName = 'myra:bill';
    private $params;

    public function __construct(ParameterBagInterface $params, string $name = null)
    {
        $this->params = $params;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a Bill')
            ->addOption('default', 'd');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $default = $input->getOption('default');


        $io = new SymfonyStyle($input, $output);

        // Defaults
        $header = "SOPRADO GMBH";
        $senderAddress = "Soprado GmbH - Bavariafilmplatz 7 - 82031 Grünwald";
        $senderPhone = "089/75 40 88 30";
        $senderFax = "089/75 40 88 33";
        $senderEmail = "info@soprado";
        $dueDate = "nach Rechnungseingang";
        $signature = "Sascha Schumann";

        if ($default === false) {
            $header = $io->ask("Enter header", $header);
            $senderAddress = $io->ask("Enter sender's address", $senderAddress);
            $senderPhone = $io->ask("Enter sender's phone", $senderPhone);
            $senderFax = $io->ask("Enter sender's fax", $senderFax);
            $senderEmail = $io->ask("Enter sender's email", $senderEmail);
            $dueDate = $io->ask("Enter due date", $dueDate);
            $signature = $io->ask("Enter signature", $signature);
        }

        // file paths
        $csvFilePath = $this->params->get('kernel.project_dir') . '/public/uploads/bills/MyraCache_1.csv';
        $odsFilePath = $this->params->get('kernel.project_dir') . '/public/uploads/data/Musterkundensatz.ods';
        $odtFilePath = $this->params->get('kernel.project_dir') . '/public/uploads/data/Template_Rechnung_Deutschland.odt';

        $bills = new BillCalculator($csvFilePath, $odsFilePath);

        $result = $bills->calculate();

        $renr = $result['year'] . "-" . strtoupper($result['data']['%SHORTCUSTOMER%'][0]) . rand(100, 1000);
        if ($default===false){
            $renr = $io->ask("Enter bill's number", $renr);
        }

        $odt = new ODTChanger($odtFilePath);
        $end = $odt->generate(
            $header,
            $senderAddress,
            $result['data']['%ANSCHRIFT-ZEILE1%'],
            $result['data']['%ANSCHRIFT-ZEILE2%'],
            $result['data']['%ANSCHRIFT-ZEILE3%'],
            $result['data']['%ANSCHRIFT-ZEILE4%'],
            $senderPhone,
            $senderFax,
            $senderEmail,
            $dueDate,
            $signature,
            $result['data']['%DOMAIN%'],
            $result['month'],
            $result['count'],
            $result['data']['%EINHEITSPREIS%'],
            $result['data']['%MWST% '],
            $renr,
            (new \DateTime())->format('d.m.Y'),
            $result['sum']
        );
        if ($end){
            $io->success("ODT file created at: ".preg_replace('/\/[\w_]+\.odt/', '/'.$result['data']['%DOMAIN%'].'.odt', $odtFilePath) );
        }else{
            $io->error("Could not generate ODT file");
            die(0);
        }

        $pdfFilePath = preg_replace('/\/[\w_]+\.odt/', '/'.$result['data']['%DOMAIN%'].'.pdf', $odtFilePath);
        $pdf = new PDFGenerator($pdfFilePath);
        $pdfResult = $pdf->generate(
            $header,
            $senderAddress,
            $result['data']['%ANSCHRIFT-ZEILE1%'],
            $result['data']['%ANSCHRIFT-ZEILE2%'],
            $result['data']['%ANSCHRIFT-ZEILE3%'],
            $result['data']['%ANSCHRIFT-ZEILE4%'],
            $senderPhone,
            $senderFax,
            $senderEmail,
            $dueDate,
            $signature,
            $result['data']['%DOMAIN%'],
            $result['month'],
            $result['count'],
            $result['data']['%EINHEITSPREIS%'],
            $result['data']['%MWST% '],
            $renr,
            (new \DateTime())->format('d.m.Y'),
            $result['sum']
        );

        if ($pdfResult){
            $io->success("PDF file created at: ".$pdfFilePath);
        }
        return 0;
    }
}
